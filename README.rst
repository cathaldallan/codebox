codebox - Python utility code collection.
-----------------------------------------

Installation
==================

.. code-block:: bash

    pip install codebox


Documentation
=============

https://cathaldallan.gitlab.io/codebox
